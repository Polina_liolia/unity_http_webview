﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace UniWebServer
{
    [RequireComponent(typeof(EmbeddedWebServerComponent))]
    public class FileServer : MonoBehaviour, IWebResource
    {
        public string folderPath = "/hextris";
        EmbeddedWebServerComponent server;

        void Start()
        {
            server = GetComponent<EmbeddedWebServerComponent>();
            server.AddResource(folderPath, this);
            Debug.Log("File server started");

            //Polina: just for test - can be removed
            //StartCoroutine(SendRequest());

        }

        //Polina: just for test - can be removed
        private IEnumerator SendRequest()
        {
            string uri = "http://localhost:8079/"  + folderPath + "/index.html";
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri)) 
            {
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                string[] pages = uri.Split('/');
                int page = pages.Length - 1;

                if (webRequest.isNetworkError)
                {
                    Debug.Log(pages[page] + ": Error: " + webRequest.error);
                }
                else
                {
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                }
            }
        }

        public void HandleRequest(Request request, Response response)
        {
            // check if file exist at folder (need to assume a base local root)
            string folderRoot = Application.streamingAssetsPath;
            string fullPath = folderRoot + Uri.UnescapeDataString(request.uri.LocalPath);
            // get file extension to add to header
            string fileExt = Path.GetExtension(fullPath);
            // not found
            if (!File.Exists(fullPath)) {
                response.statusCode = 404;
                response.message = "Not Found";
                return;
            }

            // serve the file
            response.statusCode = 200;
            response.message = "OK";
            response.headers.Add("Content-Type", MimeTypeMap.GetMimeType(fileExt));

            // read file and set bytes
            using (FileStream fs = File.OpenRead(fullPath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                // add content length
                response.headers.Add("Content-Length", length.ToString());

                // use binary for mostly all except text
                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                response.SetBytes(buffer);

            }
        }

    }
}